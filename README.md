![](RackMultipart20200416-4-i23ea_html_eec58cab681f9ed9.png)

# Software Downloads

| **FILE** | **DESCRIPTION** |
| --- | --- |
| **IDs** | Contains all of the domain _Horizon_ Notes IDs referenced in the Developer&#39;s Guide and used in the various videos and applications</span>. To note: <span style="text-decoration: underline">you will need to use _black.id_ (the database manager) to initially access the applications. There are no passwords associated with these IDs. |
| **Names** | The Domino Directory for the domain _Horizon_. You will want to use this if you plan on setting up the complete _Horizon_ test environment.\* Should you wish to use web clients to access these applications, a single space (&quot; &quot;) is used as the password for all users. |
| **WA7.1.ntf** | The Workflow Ascendant master template. |
| **WAApplicationA** | This application corresponds to all of the examples in _Chapter 2 - Process: Routing documents_ of the Workflow Ascendant Developer&#39;s Guide. |
| **WAApplicationB** | This application corresponds to all of the examples in _Chapter 3 - Constraints: Specifying Users_ of the Workflow Ascendant Developer&#39;s Guide. |

\* We hope to provide a script shortly which will automatically create the mailboxes corresponding to the users contained in this directory.
